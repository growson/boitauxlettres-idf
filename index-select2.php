<?php
$dataFull = [];
$data = [];
if (($handle = fopen(__DIR__.'/laposte_hexasmal.csv', "r")) !== false) {
    $headers = [];
    while (($row = fgetcsv($handle, null, ";")) !== false) {
        if (!$headers) {
            $headers = $row;
        }
        elseif (!empty($row[1])) {
            $dataFull[$row[2]][$row[1]] = array_combine($headers, $row);
            $data[$row[2]][] = $row[1];
        }
    }
    fclose($handle);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Get City by Postcode</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
    <style>
        body {
            padding: 20px;
        }
    </style>
</head>

<body>

<?php if (!empty($_POST['postcode']) && !empty($_POST['city']) && isset($dataFull[$_POST['postcode']][$_POST['city']])) : ?>
    <?php $cityData = $dataFull[$_POST['postcode']][$_POST['city']] ?>
    <div class="container">
        <h3>City information:</h3>
        <ul class="list-group">
            <?php foreach ($cityData as $name => $value) : ?>
                <li class="list-group-item">
                    <span class="badge"><?php echo $value ?: 'n/a' ?></span>
                    <?php echo $name ?>
                </li>
            <?php endforeach ?>
        </ul>
        <a class="btn btn-default" href="/">&laquo; Back</a>
    </div><!-- /.container -->
<?php else : ?>
    <div class="container">
        <form method="post">
            <div class="form-group">
                <label for="postcode">Postcode</label>
                <select class="form-control" id="postcode" name="postcode">
                    <option></option>
                    <?php foreach (array_keys($data) as $postcode) : ?>
                        <option value="<?php echo $postcode ?>"><?php echo $postcode ?></option>
                    <?php endforeach ?>
                </select>
            </div>
            <div class="form-group">
                <label for="city">City</label>
                <select class="form-control" id="city" name="city"></select>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div><!-- /.container -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
    <script>
        var data = <?php echo json_encode($data) ?>;
        $(document).ready(function() {
            $('#city').select2({
                placeholder: 'Select City',
                allowClear: true
            });
            $('#postcode').select2({
                placeholder: 'Select Postcode',
                allowClear: true
            }).on('change', function() {
                var cities = data[$(this).val()];
                var options = [];
                for (var i in cities) {
                    options.push($('<option>', {value: cities[i], text: cities[i]}));
                }
                $('#city').html(options).val(null);
            }).val(<?php echo isset($_POST['postcode']) ? $_POST['postcode'] : 'null' ?>).trigger("change");
        });
    </script>
<?php endif ?>
</body>
</html>
