<?php
$dataFull = [];
$data = [];
if (($handle = fopen(__DIR__.'/laposte_hexasmal.csv', "r")) !== false) {
    $headers = [];
    while (($row = fgetcsv($handle, null, ";")) !== false) {
        if (!$headers) {
            $headers = $row;
        }
        elseif (!empty($row[1])) {
            $dataFull[$row[2]][$row[1]] = array_combine($headers, $row);
            $data[$row[2]][] = $row[1];
        }
    }
    fclose($handle);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Get City by Postcode</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet">
    <style>
        body {
            padding: 20px;
        }

        .ui-autocomplete {
            max-height: 100px;
            overflow-y: auto;
            overflow-x: hidden;
        }

        * html .ui-autocomplete {
            height: 100px;
        }
    </style>
</head>

<body>

<?php if (!empty($_POST['postcode']) && !empty($_POST['city']) && isset($dataFull[$_POST['postcode']][$_POST['city']])) : ?>
    <?php $cityData = $dataFull[$_POST['postcode']][$_POST['city']] ?>
    <div class="container">
        <h3>City information:</h3>
        <ul class="list-group">
            <?php foreach ($cityData as $name => $value) : ?>
                <li class="list-group-item">
                    <span class="badge"><?php echo $value ?: 'n/a' ?></span>
                    <?php echo $name ?>
                </li>
            <?php endforeach ?>
        </ul>
        <a class="btn btn-default" href="/">&laquo; Back</a>
    </div><!-- /.container -->
<?php else : ?>
    <div class="container">
        <form method="post">
            <div class="form-group">
                <label for="postcode">Postcode</label>
                <input type="text" class="form-control" id="postcode" name="postcode" placeholder="Postcode" required>
            </div>
            <div class="form-group">
                <label for="city">City</label>
                <select class="form-control" id="city" name="city" required></select>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div><!-- /.container -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script>
        var data = <?php echo json_encode($data) ?>;
        var defaultSelect = $('<option>', {text: 'Select City', disabled: 'disabled', value: 0});
        $('#city').html(defaultSelect).val(0);

        var selectCallback = function(event, ui) {
            var cities = data[ui.item.value];console.log(cities);
            var options = [defaultSelect];
            for (var i in cities) {
                options.push($('<option>', {value: cities[i], text: cities[i]}));
            }
            $('#city').html(options);
        };

        $(document).ready(function() {
            $("#postcode").autocomplete({
                minLength: 3,
                select: selectCallback,
                source: <?php echo json_encode(array_map(function($value) { return "$value"; }, array_keys($data))) ?>
            }).on('keyup', function(event) {
                if ($(this).val().length == 5) {
                    selectCallback(event, {
                        item: {
                            value: $(this).val()
                        }
                    });
                } else {
                    $('#city').html(defaultSelect).val(0);
                }
            });
        });
    </script>
<?php endif ?>
</body>
</html>
